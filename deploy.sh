#!/bin/bash

if [ ! -d /usr/share/nginx/html ]; then
mkdir -p /usr/share/nginx/html
fi

# Clean previous version
cd /usr/share/nginx/html
rm -rf wpd
[[ $? -eq 0 ]] || { echo "ERROR"; exit 12; };

mkdir -p /usr/share/nginx/html/wpd

# Unpack the current version
tar -xzvf /usr/share/nginx/webplotdigitizer.tar.gz -C /usr/share/nginx/html/wpd
[[ $? -eq 0 ]] || { echo "ERROR"; exit 14; };

# Set ownership
chown -R root.root /usr/share/nginx/html/wpd
[[ $? -eq 0 ]] || { echo "ERROR"; exit 16; };

# Clean
rm -f /usr/share/nginx/webplotdigitizer.tar.gz

systemctl restart nginx