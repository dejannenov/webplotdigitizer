var wpd = wpd || {};

wpd.boCore = (function () {
    DIGITIZED_DATA_CLASS = null;
    API_BASE_URL = "https://ops-dev.resonant.us/api/v1";

    async function saveMetric(dataSet) {
        if (DIGITIZED_DATA_CLASS) {
            const attributes = DIGITIZED_DATA_CLASS.attributes.reduce((obj, item) => {
                switch (item.name) {
                    case "Name":
                        obj[item.id] = `Digitizet_Data_${Date.now()}`;
                        break;
                    case "Coordinats":
                        obj[item.id] = JSON.stringify({
                            coordinates: dataSet[0]._dataPoints.map(item => {
                                return {
                                    x: item.x,
                                    y: item.y
                                }
                            })
                        })
                        break;
                    default:
                        obj[item.id] = null;
                }
                return obj
            }, {})

            const dto = {
                tags: [],
                attributes: attributes,
                toRelationships: [],
                fromRelationships: []
            };

            const response = await fetch(`${API_BASE_URL}/isn/boi/${DIGITIZED_DATA_CLASS.id}?dataView=full`, {
                method: "POST",
                credentials: "include",
                headers: {
                    'Content-Type': 'application/json'
                },
                body: JSON.stringify(dto)
            })
                .then(r => r.json().then(data => ({data: data, status: r.status})))
                .then(response => {
                    if (response.status === 200) {
                        window.alert("Digitized data created successfully.");
                        attachFile(response.data);
                        alert("The digitized data image is attaching to created instance. That could take a while.");
                    } else {
                        window.alert("Something went wrong.");
                    }
                });
        }
    }

    function getDigitizetDataClass() {
        fetch(`${API_BASE_URL}/isn/boc/all`, {
            method: "GET",
            credentials: "include"
        })
            .then(r => r.json().then(data => data))
            .then(data => {
                DIGITIZED_DATA_CLASS = data.find(item => item.name.toLowerCase() === "digitized data");
            })
            .catch(e => {
                console.error(e);
            })
    }

    function attachFile(instance) {
        const imageUrl = getDigitizedImage();
        const file = dataURLtoFile(imageUrl, 'image.png');
        const formData = new FormData();
        formData.append('attachment', file);
        fetch(`${API_BASE_URL}/isn/boi/${instance.id}/file/upload?instanceName=${file.name}`, {
            method: "POST",
            credentials: "include",
            body: formData
        }).then(response => {
            console.warn(response)
        })
    }

    function getDigitizedImage() {
        const mainCanvas = document.getElementById('mainCanvas');
        const dataCanvas = document.getElementById('dataCanvas');

        const context = mainCanvas.getContext('2d');
        context.drawImage(dataCanvas, 0, 0);

        return mainCanvas.toDataURL("image/png");
    }

    function dataURLtoFile(dataurl, filename) {
        var arr = dataurl.split(','), mime = arr[0].match(/:(.*?);/)[1],
            bstr = atob(arr[1]), n = bstr.length, u8arr = new Uint8Array(n);
        while (n--) {
            u8arr[n] = bstr.charCodeAt(n);
        }
        return new File([u8arr], filename, {type: mime});
    }

    getDigitizetDataClass();
    return {
        saveMetric: saveMetric,
        API_BASE_URL: API_BASE_URL
    }
})();